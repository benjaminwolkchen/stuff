## Erdkunde für den 21.05.2021
## Nummer 1: Erläutere die Unterschiede zwischen Flucht und Migration

Man spricht von Flucht, wenn Menschen mit Aussicht auf Rückkehr wegen zum Beispiel Krieg oder Naturkatastrophen ihre Heimat verlassen.

Wenn man von Migration spricht, dann meint man dass die Menschen dauerhaft ihren Wohnort wechseln.
Migration kann innerhalb eines Landes geschehen, dann nennt man sie Binnenmigration.
Wenn die Migration außerhalb von Landesgränzen geschieht, dann nennt man sie transnationale Migration.

## Nummer 2a + b) Nenne fünf Regionen aus denen besonders viele Flüchtlinge kommen und recherchiere die Gründe für Fluchtbewegungen

1. Afghanistan: Seit 19 Jahren andauernder Krieg
2. Myanmar: Gewalt und Konflikte von der Armee
3. Sudan: Ein Bürgerkrieg seid 2013
4. Nigeria: Terroranschlang von Boko Haram und Menschenrechtsverletzungen durch Sicherheitskräfte
5. Kaukasus: Krieg, Freiheitsberaubung, Folter

## Nummer 2c) Nenne die Regionen die besonders viele Flüchtlinge aufnehmen

1. Mittel- und Westeuropa
2. Nordamerika

## Nummer 2d) Benenne die Hauptzielregionen der Wirtschaftsmigranten. Gehe dabei auf den Ausbildungsstand der Migranten ein.
Die qualifizierten Arbeitskräfte wander überwiegend nach Nordamerika und Mittel- und Westeuropa ein.
Die weniger qualifizierten Arbeitskräfte wandern überwiegend nach Australien und Neuseeland ein.