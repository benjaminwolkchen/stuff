# Beziehungen und Partnerschaften

## 1. Wie sieht für dich der perfekte Start in eine Partnerschaft aus? Was wüsste zuvor gelaufen sein und wie genau sollte die Partnerschaft beginnen?

Erstmal eine Nacht drüber schlafen :O und mit Gott Reden, sich Gott anvertrauen, seine Meinung einholen. Wenn alles gut ist, viel Zeit miteinander verbringen, Pläne schmieden, heiraten.

## 2. Nenne die für dich fünf wichtigsten Kriterien, die dein Partner/deine Partnerin erfüllen sollte.

1. Beziehung mit Gott

2. Offenheit: wenn ich mal nervig bin, will ich das auch wissen :D

3. Ähnliche / sich ergänzende Interessen, Lebenswunsch, Traum

4. Emotionaler Support

5. Hat guten Musikgeschmack ;)