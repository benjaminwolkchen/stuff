# The three day koala cuddle adventure that only lasted one.

"Have you packed your backpack yet, Liz?" you can hear the voice of mom from the kitchen.
Mom was just as excited as we where.
It was a sunny day and we were about to hit the road for a three-day camping trip through the dominican jungle.
My sister - Liz - saw it in a video once and since then, she wanted to go so badly. For her twelfth birthday, our mom booked the trip.
"Yes, I am almost finished. I only need my raincoat - have you seen it?", my sister shouted back.

A few minutes later, we were all ready and sitting in the car to the airport. On the radio some chart music blasts.
It was a hot and sunny day and I just fell asleep.

When I woke up, my neck hurt and I had a little headache. Oddly enough, I didn't remember a single thing from the whole ride.
As we arrived at the tour spot, Liz was happier than ever.
She imagined all the things that we would do that day.

"And then I want to pet a koala, and boop his little nose. And I want to get some bamboo and plant a bamboo tree when we get home."
If our tour guide didn't arrive, she would have kept talking, and talking, and talking.
At that point I was a little exhausted and annoyed.

Our tour guide was a short guy with blonde hair. if he had not introduced himself, I wouldn't have recognized him.
"You seem to be family Müller."
My mom nods friendly and says: "Yes, thats certainly true. You must be our tour guide?"
He responds with: "My name is Matthew Moore," he could see in our eyes, how excited we were, so he didn't waste any time: "Let's start the tour. Follow me!"

The next few minutes, he guided us through a hilly landscape. He talked a lot about the people that lived here once, but as there were much more exciting things to think about, I just zoned out.
I'd could have been a good hour in, as I awaken from my thoughts. More precisely, I was woken up, by the scream of excitement of my sister.
We were already a few meteres in the jungle, and she just saw a really pretty plant.
"That looks like vanilla, can I eat it?" she asked, excitedly.
"No, never. It may look pretty - and like vanilla, but shouldn't eat it. In this part of the jungle, there are a lot of animals that can sit on plants and when you eat them, you'll get really sick." our tour guide explained.
We started walking again, and I zoned out again. A few times, I've stopped to admire the beautiful plants, but at that point I was just tired.
Suddenly I hear someone shouting. I didn't think anything of it, but then I heard another, and another.
As I slowly return to normal life, I see it.
I freeze, even though it is like 40 degrees. I've never felt such frostiness. I get chills all over.
I pinch myself. Is this real? Apparently it was.
A 19 feet big alligator. One feet between him and us.
You know the moments in movies or books, when a character is about to die, and thier life flashes before them. That's what happened to me.
I wasn't even done processing this, the next big bomb hits. Where is our tour guide?
Don't say we've lost him.
As first moment of freezing shock was over, we slowly grasped what that meant. We were on our own now.
I tried slowly moving backwards, so I don't scare the alligator.
One foot after another, now: Don't fall.
Somehow we made it far enough away from the animal.
And there he was: Our Tour guide, inspecting plants.
"Should we contiue?" he asked us happily.
We all unanimously shouted: "No!!!"
So we went back home.
The camping trip that was supposed to last three days only lasted one.
Liz didn't get to see - or cuddle - a koala, the whole reason why we even went on the trip.
Even though we completed none of our goals, we still felt, it was a successful trip. We didn't really talk about it afterwards, but we all felt happy.